const { expect } = require("chai");

/** Basic arithmetic operations */
const mylib = {
    /** Multiline arrow function. */
    add: (a,b) => {
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },
    /** Singleline arrow function. */
    divide: (divident,divisor) => {
    result = divident / divisor;
    if(isFinite(result)) {
        return result;
    } else {
        return "Cannot divide by zero"
    }},
    /** Regular function. */
    multiply: function(a, b) {
        return a * b;
    }
};

module.exports = mylib;
