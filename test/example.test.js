const expect = require('chai').expect;
const assert = require('chai').assert;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {

    after(() => 
    // Cleanup
    // For example: shutdown the Express server.
    console.log("After mylib tests"));
    it("Can add 1 and 2 together", () => {
    // Tests
    expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3, for some reason?");
});
it("Can subtract 5 from 7", () => {
    // Tests
    expect(mylib.subtract(7,5)).equal(2, "7 - 5 is not 2, for some reason?");
});
it("Can't divide any number with zero", () => {
    // Tests
    expect(mylib.divide(5,0)).equal("Cannot divide by zero", "5 / 0 is not Infinity, for some reason?");
});
it("Can divide 6 by two", () => {
    // Tests
    expect(mylib.divide(6,2)).equal(3, "6 / 2 is not 3, for some reason?");
});
it("Can multiply 1 with 2", () => {
    // Tests
    expect(mylib.multiply(1,2)).equal(2, "1 * 2 is not 2, for some reason?");
});

    before(() => {
        // initialization
        // create objects... etc..
        console.log("Initialising tests");
    });
    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar');
        })
    
});